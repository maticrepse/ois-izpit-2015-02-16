var express = require('express');

var app = express();
app.use(express.static(__dirname + '/public'));


app.get('/', function(req, res) {
    res.sendfile(__dirname + '/public/seznam.html');
});



app.get('/api/seznam', function(req, res) {
	res.send(uporabnikiSpomin);
});


/**
 * TODO: Potrebna je implementacija tega dela funkcionalnosti (PU Dodajanje osebe)
 */
app.get('/api/dodaj', function(req, res) {
	//https://ois-izpit-2015-02-16-maticrepse.c9.io/api/dodaj?ds=&ime=&priimek=&ulica=&hisnaStevilka=&postnaStevilka=5270&kraj=Ajdovščina&drzava=&poklic=&telefonskaStevilka=
	var davcna = req.query.ds;
	var ime = req.query.ime;
	var priimek = req.query.priimek;
	var naslov = req.query.ulica;
	var hisnaStevilka = req.query.hisnaStevilka;
	var postnaStevilka = req.query.postnaStevilka;
	var kraj = req.query.kraj;
	var drzava = req.query.drzava;
	var poklic = req.query.poklic;
	var telefonskaStevilka = req.query.telefonskaStevilka;
	var stevec=0;
	for(var i in uporabnikiSpomin){
		stevec++;
	}
	if(davcna&&ime&&priimek&&naslov&&hisnaStevilka&&postnaStevilka&&kraj&&drzava&&poklic&&telefonskaStevilka){
		uporabnikiSpomin[stevec]={davcnaStevilka: davcna, ime: ime, priimek: priimek, naslov: naslov, hisnaStevilka: hisnaStevilka, postnaStevilka: postnaStevilka, kraj: kraj, drzava: drzava, poklic: poklic, telefonskaStevilka: telefonskaStevilka};
		res.redirect('https://ois-izpit-2015-02-16-maticrepse.c9.io');
	}else{
		res.send("Podatki so nepopolni, oseba ni bila dodana. <a href='javascript:window.history.back()'>Nazaj</a>");
	}
});


/**
 * TODO: Potrebna je implementacija tega dela funkcionalnosti (PU Brisanje osebe)
 */
app.get('/api/brisi', function(req, res) {
	// ...
	//res.send('Potrebno je implementirati brisanje oseb!');
	// ...
	var id = req.query.id;
	if(id){
		var bool = false;
		console.log(id+"---------------------------------");
		for(var i in uporabnikiSpomin){
			var davcna = uporabnikiSpomin[i]["davcnaStevilka"];
			console.log(davcna);
			if(davcna == id){
				uporabnikiSpomin.splice(i, 1);
				res.redirect('https://ois-izpit-2015-02-16-maticrepse.c9.io');
				bool=true;
			}
		}
		if(!bool){
			res.send("Oseba z davčno številko "+id+" ne obstaja.<br/><a href='javascript:window.history.back()'>Nazaj</a>");
		}
	}else{
		res.send("Napačna zahteva!");
	}
	
	
});


var port = process.env.PORT || 3030;
app.listen(port);
console.log('Streznik tece na ' + port + ' ...');


var uporabnikiSpomin = [
	{davcnaStevilka: '98765432', ime: 'James', priimek: 'Blond', naslov: 'Vinska cesta', hisnaStevilka: '13', postnaStevilka: '2000', kraj: 'Maribor', drzava: 'Slovenija', poklic: 'POTAPLJAČ', telefonskaStevilka: '(958) 309 007'}, 
	{davcnaStevilka: '12345678', ime: 'Ata', priimek: 'Smrk', naslov: 'Sračji dol', hisnaStevilka: '15', postnaStevilka: '1000', kraj: 'Ljubljana', drzava: 'Slovenija', poklic: 'GRADBENI DELOVODJA', telefonskaStevilka: '(051) 690 107'}
];